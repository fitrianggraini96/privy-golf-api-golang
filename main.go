package main

import (
	"privy-golf-api-golang/database"
	"privy-golf-api-golang/router"
)

func main() {
	database.StartDB()

	var PORT = ":8000"

	router.StartServer().Run(PORT)
}
