package helpers

type response struct {
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func Response(status string, message string, data interface{}) *response {
	return &response{
		Status:  status,
		Message: message,
		Data:    data,
	}
}
