package middleware

import (
	"privy-golf-api-golang/database"
	"privy-golf-api-golang/models"

	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/dgrijalva/jwt-go"
)

func UserAuthorization() gin.HandlerFunc {
	return func(c *gin.Context) {
		db := database.GetDB()

		userDataInterface := c.MustGet("userData")
		var userID uint
		if userData, ok := userDataInterface.(jwt.MapClaims); ok {
			userIDFloat, ok := userData["id"].(float64)
			if !ok {
			}
			userID = uint(userIDFloat)
		} else {
		}

		User := models.User{}
		if c.Request.Method != "POST" {
			err := db.First(&User, "id = ?", userID).Error
			if err != nil {
				c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
					"error":   "Data Not Found",
					"message": "Data doesn't exist",
				})
				return
			}

			if uint(User.Id) != userID {
				c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"error":   "Unauthorized",
					"message": "You are not allowed to access this data",
				})
				return
			}
		}

		c.Next()
	}
}
