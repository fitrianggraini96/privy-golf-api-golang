package models

import (
	"time"

	"privy-golf-api-golang/utils/helpers"

	"github.com/google/uuid"
	"gorm.io/gorm"
	//google validator
)

type User struct {
	Id        uint           `gorm:"primaryKey" json:"id"`
	Uuid      uuid.UUID      `gorm:"column:uuid;type:char(255)" json:"uuid"`
	PrivyID   *string        `gorm:"column:privy_id;type:character varying(255)" json:"privy_id"`
	Name      string         `gorm:"column:name;type:character varying(255);not null" json:"name"`
	Email     string         `gorm:"column:email;type:character varying(255);not null;uniqueIndex:user_email_key,priority:1" json:"email"`
	Password  string         `json:"password" gorm:"not null" form:"password"`
	CreatedAt time.Time      `gorm:"column:created_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time      `gorm:"column:updated_at;type:timestamp;default:CURRENT_TIMESTAMP" json:"updated_at"`
	ExpiredAt *time.Time     `gorm:"column:expired_at;type:timestamp" json:"expired_at"`
	DeletedAt gorm.DeletedAt `gorm:"column:deleted_at;type:timestamp" json:"deleted_at"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.Password = helpers.HashPass(u.Password)
	err = nil
	return
}
