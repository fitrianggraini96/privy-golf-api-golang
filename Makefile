dev:
	go run main.go

migrate:
	@source .env && migrate -path database/migrations -database "$$DB_DSN" up

seeder:
	@source .env && migrate -path database/seeders -database "$$DB_DSN" up