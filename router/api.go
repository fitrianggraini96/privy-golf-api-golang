package router

import (
	"privy-golf-api-golang/app/controllers"
	"privy-golf-api-golang/app/repository"
	"privy-golf-api-golang/app/service"
	"privy-golf-api-golang/middleware"

	"github.com/gin-gonic/gin"
)

func StartServer() *gin.Engine {
	router := gin.Default()

	userRepository := repository.NewUserRepository()
	userService := service.NewUserService(userRepository)
	userController := controllers.NewUserController(userService)

	userRouter := router.Group("/users")
	{
		userRouter.POST("/register", userController.UserRegister)
		userRouter.POST("/login", userController.UserLogin)

		userRouter.DELETE("/:userId", middleware.UserAuthorization(), userController.DeleteUser)
	}

	return router
}
