package controllers

import (
	"fmt"
	"net/http"
	"privy-golf-api-golang/app/service"
	"privy-golf-api-golang/models"
	"privy-golf-api-golang/utils/helpers"
	"strconv"

	"github.com/gin-gonic/gin"
)

var (
	appJSON = "application/json"
)

//Kontrak Controllernya
type UserController interface {
	UserRegister(c *gin.Context)
	UserLogin(c *gin.Context)
	DeleteUser(c *gin.Context)
}

type userControllerImpl struct {
	UserService service.UserService
}

//Inisiasi struct dengan kontrak interface
func NewUserController(newUserService service.UserService) UserController {
	return &userControllerImpl{
		UserService: newUserService,
	}
}

func (controller *userControllerImpl) UserRegister(c *gin.Context) {
	// Validate input
	var input models.User
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create user
	result, err := controller.UserService.UserRegister(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
		return
	}

	c.JSON(http.StatusOK, helpers.Response("success", "sign-in successfully", result))

}

func (controller *userControllerImpl) UserLogin(c *gin.Context) {
	contentType := helpers.GetContentType(c)
	User := models.User{}

	if contentType == appJSON {
		c.ShouldBindJSON(&User)
	} else {
		c.ShouldBind(&User)
	}

	getUser, err := controller.UserService.UserLogin(&User)

	if err != nil {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error":   "Unauthorized",
			"message": "invalid email/password",
		})
		return
	}

	comparePass := helpers.ComparePass([]byte(getUser.Password), []byte(User.Password))
	fmt.Println(comparePass)
	if !comparePass {
		c.JSON(http.StatusUnauthorized, gin.H{
			"error":   "Unauthorized",
			"message": "invalid email/password",
		})
		return
	}

	token := helpers.GenerateToken(uint(getUser.Id), User.Email)

	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func (controller *userControllerImpl) DeleteUser(c *gin.Context) {
	userId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Parameter False"})
		return
	}

	res, err := controller.UserService.DeleteUser(uint(userId))
	_ = res
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Your account has been successfully deleted",
	})
}
