package repository

import (
	"privy-golf-api-golang/database"
	"privy-golf-api-golang/models"

	"github.com/google/uuid"
)

type UserRepository interface {
	UserRegister(userReq *models.User) (*models.User, error)
	UserLogin(userReq *models.User) (*models.User, error)
	DeleteUser(id uint) (*models.User, error)
}

type userRepositoryImpl struct{}

//Inisiasi struct dengan kontrak interface
func NewUserRepository() UserRepository {
	return &userRepositoryImpl{}
}

func (repository *userRepositoryImpl) UserRegister(UserReq *models.User) (*models.User, error) {
	var db = database.GetDB()

	newUUID, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	User := models.User{
		Uuid:     newUUID,
		Name:     UserReq.Name,
		Email:    UserReq.Email,
		Password: UserReq.Password,
	}

	err = db.Create(&User).Error

	if err != nil {
		return nil, err
	}

	return &User, err

}

func (repository *userRepositoryImpl) UserLogin(UserReq *models.User) (*models.User, error) {
	var db = database.GetDB()

	user := models.User{}

	err := db.First(&user, "email = ?", UserReq.Email).Take(&user).Error

	if err != nil {
		return nil, err
	}

	return &user, err

}

func (repository *userRepositoryImpl) DeleteUser(id uint) (*models.User, error) {
	db := database.GetDB()
	User := models.User{}
	err := db.Delete(User, uint(id)).Error

	if err != nil {
		return nil, err
	}

	return &User, err
}
